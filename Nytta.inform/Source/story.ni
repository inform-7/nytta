"Nytta" by "Andreas Skyman and Stian Rødven Eide"

The story headline is "An Adventure with the Doctor".

There is a room called Inside the TARDIS. The description of Inside the TARDIS is "Time And Relative Dimensions In Space, or TARDIS for short, is the vessel with which you and the Doctor travel through time and space." The printed name of Inside the TARDIS is "Inside the TARDIS".

Report listening when the player is in Inside the TARDIS:
	Say "A discrete humming fills the room. It is rather soothing." instead.

A control console is in Inside the TARDIS. It is fixed in place. The description of the control console is "A control console. Its a complicated mess of buttons, lights and switches, with a centre pillar that pulsates faintly."

A woman called the Doctor is in Inside the TARDIS. The indefinite article of the Doctor is "the". The description of the Doctor is "A time lord of many guises, the Doctor is currently a smart-looking woman in a brown overcoat."

A brown overcoat is a wearable container. It is closed. The Doctor is wearing the brown overcoat.

Every turn when the player is in Inside the TARDIS and the Doctor is in Inside the TARDIS:
	Say "The Doctor [one of]fiddles with the control mechanics[or]rummages around in her toolbox, looking for something[or]seems to be thinking very hard about something[or]talks to herself[or]is talking softly to the TARDIS[or]makes wild gestures to the TARDIS[as decreasingly likely outcomes]." 

Instead of doing something other than examining with the control console:
	Say "You're right where you want, or perhaps need, to be. There's no need to mess around with the controls at the moment."

The wooden exit door is west of Inside the TARDIS. It is a lockable scenery door. It is closed and locked. Through it is The Pavilion. The printed name of the wooden exit door is "door of the TARDIS". The TARDIS-unlocking key unlocks the wooden exit door. The indefinite article of the TARDIS-unlocking key is "the". The printed name of the TARDIS-unlocking key is "TARDIS key".

The TARDIS-unlocking key is in the brown overcoat.

The Pavilion is a room.

A TARDIS-box is a vehicle in the Pavilion. The indefinite article of the TARDIS-box is "the". The printed name of the TARDIS-box is "TARDIS". The description of the TARDIS-box is "A police box from 1950s Earth, United Kingdom." Understand "tardis" as the TARDIS-box.

Rule for printing the name of the TARDIS-box:
	omit contents in listing;
	continue the action.

Instead of entering the TARDIS-box:
	say "You enter the TARDIS.[paragraph break]";
	Now the player is in Inside the TARDIS.
	
Inside from the Pavilion is Inside the TARDIS.
Instead of going inside from the Pavilion:
	say "You enter the TARDIS.[paragraph break]";
	Now the player is in Inside the TARDIS.

Instead of going outside from Inside the TARDIS:
	try entering the wooden exit door.
	
Rule for printing the name of the wooden exit door:
	say "door of the TARDIS";
	now the TARDIS-unlocking key is known.

Instead of interrogating the Doctor about the TARDIS-unlocking key when the TARDIS-unlocking key is in the brown overcoat:
	say "'Ah, the key, yes, of course! Here you go.'";
	now the player is carrying the TARDIS-unlocking key.
	
Before going through the wooden exit door:
	If the wooden exit door is closed and the wooden exit door is locked:
		if the player is carrying the TARDIS-unlocking key:
			say "(first unlocking the door of the TARDIS with the key, then opening the door)[paragraph break]";
			now the wooden exit door is unlocked;
			now the wooden exit door is open.
			
A TARDIS-object is scenery in Inside the TARDIS. The description of the TARDIS-object is "It's certainly bigger on the inside!" Understand "TARDIS" as the TARDIS-object. The printed name of the TARDIS-object is "TARDIS". The indefinite article of the TARDIS-object is "the".

[Testing a method for conversation with the Doctor. Any items the player can ask about should be listed in Table of Doctor Items, but only seen items will be met with a response. Other topics for conversation should be listed in Table of Doctor Topics.
The downside to this (so far at least) is that only one response is implemented for any given thing or topic.]
 	
A thing can be known or unknown. A thing is usually unknown. The wooden exit door is known. The TARDIS-object is known.

Understand "ask [someone] about [any known thing]" as interrogating it about. interrogating it about is an action applying to two visible things.
Understand the commands "show" and "display" and "present" as something new.
Understand "show [something] to [someone]" or "display [something] to [someone]" or "present [something] to [someone]" as interrogating it about (with nouns reversed). Understand "show [someone] [something]" as interrogating it about.
Before printing the name of something (called the target): now the target is known.
Carry out interrogating someone about something: 
	say "There is no reply."
	
Instead of asking the Doctor about a topic listed in the Table of Doctor Topics: 
	say "[reply entry][paragraph break]".
Instead of interrogating the Doctor about an item listed in the Table of Doctor Items: 
	say "[reply entry][paragraph break]".
	
Table of Doctor Items
item	reply
control console	"'It's a console'"
doctor	"'Me? I'm just the Doctor.'"
wooden exit door	"'That usually leads outside.'"
TARDIS-object	"'She has some slight hiccups at the moment.'"

Table of Doctor Topics
topic	reply
"herself"	"'Me? I'm just the Doctor.'"